package com.viettelimex.common.config;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.Arrays;

public class KafkaTemplate {

//    public static ConsumerRecords listen(String topic) {
//        Consumer<Long, String> consumer = ConsumerCreator.createConsumer();
//        consumer.subscribe(Arrays.asList(topic));
//        ConsumerRecords<Long, String> consumerRecords = consumer.poll(Duration.ofMinutes(1));
//        consumer.commitAsync();
//        consumer.close();
//        return consumerRecords;
//    }

    public static ConsumerRecords listen(String topic, Integer partition) {
        Consumer<Long, String> consumer = ConsumerCreator.createConsumer();
        consumer.subscribe(Arrays.asList(topic));
        consumer.assign(Arrays.asList(new TopicPartition(topic, partition)));

        ConsumerRecords<Long, String> consumerRecords = consumer.poll(Duration.ofMinutes(1));
        consumerRecords.forEach(record -> {
            System.out.println("Record: " + record);
        });
        consumer.commitAsync();
        consumer.close();
        return consumerRecords;
    }

    public static void sendMessage(String topic, String data) {
        Producer<String, String> producer = ProducerCreator.createProducer();
        ProducerRecord<String, String> record = new ProducerRecord<>(topic, data);
        producer.send(record);
    }

    public static void sendMessage(String topic, Integer partition, String key, String data) {
        Producer<String, String> producer = ProducerCreator.createProducer();
        ProducerRecord<String, String> record = new ProducerRecord<>(topic, partition, key, data);
        producer.send(record);
    }
}
