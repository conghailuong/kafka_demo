package com.viettelimex.common.dto;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "stock")
public class Stock {

    @Id
    private String stockId;

    private String stockName;

    private String locate;
}
