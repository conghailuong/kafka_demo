package com.viettelimex.common.dto;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "product")
public class Product {

    @Id
    private String productId;

    private String productName;

    private String productCode;

    private Integer quantity;

    private String stockId;
}
