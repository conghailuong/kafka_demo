package com.viettelimex.common.dto;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "orders")
@ToString
public class Order {

    @Id
    public String orderId;

    public String orderName;

    public String orderCode;

    public String orderStatus;

    public String productId;

    public String ShippingId;

    public String orderAt;
}
