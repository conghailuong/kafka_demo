package com.viettelimex.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity(name = "shipping")
@AllArgsConstructor
@NoArgsConstructor
public class Shipping {

    @Id
    private String shippingId;

    private String ShippingName;

    private String ShippingCode;

    private Date ShippingAt;
}
