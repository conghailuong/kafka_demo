package com.viettelimex.common.constant;

public class KafkaConstants {

    public static String KAFKA_BROKERS = "localhost:9092";

    public static String GROUP_ID_CONFIG = "viettlimex";

    public static String GROUP_ID_CONFIG_1 = "viettlimex_1";

    public static String GROUP_ID_CONFIG_2 = "viettlimex_2";

    public static Integer MAX_NO_MESSAGE_FOUND_COUNT = 100;

    public static String OFFSET_RESET_LATEST = "latest";

    public static String OFFSET_RESET_EARLIER = "earliest";

    public static Integer MAX_POLL_RECORDS = 1;

    public static Integer MAX_POLL_INTERVAL_MS_CONFIG = 300000;

    public static Integer SESSION_TIMEOUT_MS_CONFIG = 10000;
}
