package com.viettelimex.common.constant;

public class DateTimeConstants {

    public final static String YYYY_MM_DD = "yyyy-MM-dd";
    public final static String YYYY_MM_DD_HYPHEN = "yyyy-MM-dd HH:mm:ss";
}
