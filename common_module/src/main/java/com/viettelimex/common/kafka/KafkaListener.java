package com.viettelimex.common.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.event.ListenerContainerIdleEvent;
import org.springframework.kafka.listener.AbstractConsumerSeekAware;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.listener.RecordInterceptor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class KafkaListener extends AbstractConsumerSeekAware implements MessageListener<String, String>,
        RecordInterceptor<String, String> {

    private ConsumerRecord<String, String> lastRecord;

    @Override
    public void onMessage(ConsumerRecord<String, String> record) {
        System.out.println("record: " + record);
    }

    @EventListener
    public void listen(ListenerContainerIdleEvent event) {
        System.out.println("event: " + event);
        System.out.println("Last Record: " + this.lastRecord);
//      getSeekCallbacks().forEach((tp, callback) -> callback.seekRelative(tp.topic(), tp.partition(), -1, true));
    }

    @Override
    @Nullable
    public ConsumerRecord<String, String> intercept(ConsumerRecord<String, String> record) {
        return this.lastRecord = record;
    }
}
