package com.viettelimex.common.kafka;


import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

public class LogMarker {
    public static Marker getMarker() {
        return MarkerManager.getMarker("kafkaLogging");
    }
}
