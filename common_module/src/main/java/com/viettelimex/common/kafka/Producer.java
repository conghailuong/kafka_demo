package com.viettelimex.common.kafka;

import com.viettelimex.common.constant.DateTimeConstants;
import com.viettelimex.common.util.DateTimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
public class Producer<T> {

    @Autowired
    private KafkaTemplate<String, T> kafkaTemplate;

    public void sendMessage(String topic, T message) {
        ListenableFuture<SendResult<String, T>> result = kafkaTemplate.send(topic, message);
        listenSentMess(result);
    }

    public void sendMessage(String topic, Integer partition, T message) {
        String timeStamp = DateTimeUtils.getCurrentDateString(DateTimeConstants.YYYY_MM_DD_HYPHEN);
        ListenableFuture<SendResult<String, T>> result = kafkaTemplate.send(topic,
                partition,
                timeStamp,
                message);
        listenSentMess(result);
    }

    private void listenSentMess(ListenableFuture<SendResult<String, T>> result) {
        result.addCallback(new ListenableFutureCallback<SendResult<String, T>>() {
            @Override
            public void onFailure(Throwable ex) {
                System.out.println("ex: " + ex.getMessage());
            }

            @Override
            public void onSuccess(SendResult<String, T> result) {
                System.out.println("sent at: topic: " + result.getRecordMetadata().topic() +
                        " - partition: " + result.getRecordMetadata().partition() +
                        " - offset: [" + result.getRecordMetadata().offset() + "]");
            }
        });
    }
}
