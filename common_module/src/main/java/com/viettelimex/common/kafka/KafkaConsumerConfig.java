package com.viettelimex.common.kafka;

import com.viettelimex.common.constant.KafkaConstants;
import com.viettelimex.common.dto.Order;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafka
public class KafkaConsumerConfig {

    private Map<String, Object> getConfigKafKa(String kafkaBroker, String groupId, Class<?> deValue) {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBroker);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, deValue);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, KafkaConstants.OFFSET_RESET_EARLIER);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, KafkaConstants.MAX_POLL_RECORDS);
        props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, KafkaConstants.MAX_POLL_INTERVAL_MS_CONFIG);
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, KafkaConstants.SESSION_TIMEOUT_MS_CONFIG);
        props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        return props;
    }

    // crate consumer 1

    /**
     * DefaultKafkaConsumerFactory : is used to create new Consumer instances
     * where all consumer share common configuration properties mentioned in this bean.
     *
     * @return
     */
    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> stringConsumer = getConfigKafKa(KafkaConstants.KAFKA_BROKERS, KafkaConstants.GROUP_ID_CONFIG, StringDeserializer.class);
        return new DefaultKafkaConsumerFactory(stringConsumer);
    }

    /**
     * ConcurrentKafkaListenerContainerFactory : is used to build ConcurrentMessageListenerContainer.
     * This factory is primarily for building containers for @KafkaListener annotated methods.
     *
     * @return
     */
    @Bean
    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

    // create consumer 2

    @Bean
    public ConsumerFactory<String, String> consumerObjectFactory() {
        Map<String, Object> stringConsumer = getConfigKafKa(KafkaConstants.KAFKA_BROKERS, KafkaConstants.GROUP_ID_CONFIG, StringDeserializer.class);
        return new DefaultKafkaConsumerFactory(stringConsumer);
    }

    @Bean
    public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> objectKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory();
        factory.setConsumerFactory(consumerObjectFactory());
        return factory;
    }

}
