package com.viettelimex.common.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TopicConfig {

    @Value("${erp.topic-name.order-service}")
    String orderTopic;

    @Value("${erp.topic-name.stock-service}")
    String stockTopic;

    @Value("${erp.topic-name.shipping-service}")
    String shippingTopic;

    @Bean
    public NewTopic orderTopic() {
        return new NewTopic(orderTopic, 3, (short) 1);
    }

    @Bean
    public NewTopic stockTopic() {
        return new NewTopic(stockTopic, 3, (short) 1);
    }

    @Bean
    public NewTopic shippingTopic() {
        return new NewTopic(shippingTopic, 3, (short) 1);
    }
}
