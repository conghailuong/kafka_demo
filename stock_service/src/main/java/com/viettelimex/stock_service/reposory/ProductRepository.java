package com.viettelimex.stock_service.reposory;

import com.viettelimex.common.dto.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

    @Query(value = "select * from product where product_id = :productId", nativeQuery=true)
    Product getProductById(@Param("productId") String productId);
}
