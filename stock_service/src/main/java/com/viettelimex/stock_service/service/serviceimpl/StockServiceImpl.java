package com.viettelimex.stock_service.service.serviceimpl;

import com.viettelimex.common.dto.Order;
import com.viettelimex.common.dto.Product;
import com.viettelimex.common.kafka.Producer;
import com.viettelimex.common.util.JsonUtil;
import com.viettelimex.stock_service.reposory.ProductRepository;
import com.viettelimex.stock_service.service.StockService;
import com.viettelimex.stock_service.service.base.BaseService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.stereotype.Service;

@Service
public class StockServiceImpl extends BaseService implements StockService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private Producer<String> producer;

    @Override
    public Product loadProduct(Product product) {
        return productRepository.save(product);
    }

    @Autowired
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory;

    @KafkaListener(groupId = "viettlimex_1",
            topicPartitions = {@TopicPartition(topic = "${erp.topic-name.order-service}", partitions = {"0"})})
    public void test(String value) {
        System.out.println("value: " + value);
    }

    @KafkaListener(topics = "logsdemo")
    public void testLog(String value) {
        System.out.println(value);
    }

    /**
     * listen create order event(topic = order-service, partition = 0)
     * and create fail order event(topic = order-service, partition = 2)
     *
     * @param record
     * @return
     */
    //    @KafkaListener(topics = {"create-order", "create-order-error"})
    @KafkaListener(groupId = "viettlimex", topicPartitions = {@TopicPartition(topic = "${erp.topic-name.order-service}", partitions = {"0", "2"})})
    private Product loadProduct(ConsumerRecord<?, String> record) {
        Order order = JsonUtil.convertJson2Object(record.value(), Order.class);
        if (order != null) {
            Product product = productRepository.getProductById(order.getProductId());
            Integer quantity = product != null ? product.getQuantity() : null;
            if (orderTopic.equals(record.topic()) && record.partition() == 0) {
                if (product == null || quantity <= 0) {
                    producer.sendMessage(stockTopic, 1, JsonUtil.convertObject2JsonString(order)); // fired if the product is not exist.
                    return null;
                }
                product.setQuantity(--quantity);
                producer.sendMessage(stockTopic, 0, JsonUtil.convertObject2JsonString(order)); // fired if update product successfully!
            } else if (orderTopic.equals(record.topic()) && record.partition() == 2) {
                product.setQuantity(++quantity);
            }
            return loadProduct(product); //update quantity of product when order successfully!
        }
        return null;
    }

}
