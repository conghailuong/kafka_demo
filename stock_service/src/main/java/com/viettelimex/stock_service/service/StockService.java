package com.viettelimex.stock_service.service;


import com.viettelimex.common.dto.Product;

public interface StockService {

    Product loadProduct(Product product);
}
