package com.viettelimex.order_service.service.base;

import org.springframework.beans.factory.annotation.Value;

public abstract class BaseService {

    @Value("${erp.topic-name.order-service}")
    protected String orderTopic;

    @Value("${erp.topic-name.stock-service}")
    protected String stockTopic;

    @Value("${erp.topic-name.shipping-service}")
    protected String shippingTopic;
}
