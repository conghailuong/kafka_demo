package com.viettelimex.order_service.service;


import com.viettelimex.common.dto.Order;

public interface OrderService {

    Order createOrder(Order order);

    Order updateOrder(Order order);
}
