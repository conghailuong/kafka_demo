package com.viettelimex.order_service.service.serviceimpl;

import com.viettelimex.common.dto.Order;
import com.viettelimex.common.kafka.Producer;
import com.viettelimex.common.util.JsonUtil;
import com.viettelimex.order_service.repository.OrderRepository;
import com.viettelimex.order_service.service.OrderService;
import com.viettelimex.order_service.service.base.BaseService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl extends BaseService implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private Producer<String> producer;

    @Override
    public Order createOrder(Order order) {
        Order result = orderRepository.save(order);
        producer.sendMessage(orderTopic, 0, JsonUtil.convertObject2JsonString(result)); // creating an order
        return result;
    }

    @Override
    public Order updateOrder(Order order) {
        return orderRepository.save(order);
    }

    /**
     * listen create success(topic = stock-service, partition = 0) and fail(topic = stock-service, partition = 0) order event, and
     * create success(topic = shipping-service, partition = 0) and fail(topic = shipping-service, partition = 1) shipping event
     *
     * @param record
     * @return
     */
    //    @KafkaListener(topics = {"load-product-success", "ready-for-shipping", "load-product-error", "shipping-error"})
    @KafkaListener(groupId = "viettlimex", topicPartitions = {@TopicPartition(topic = "${erp.topic-name.stock-service}", partitions = {"0", "1"}),
            @TopicPartition(topic = "${erp.topic-name.shipping-service}", partitions = {"0", "1"})})
    private Order updateOrder(ConsumerRecord<?, String> record) {
        Order order = JsonUtil.convertJson2Object(record.value(), Order.class);
        if (order != null) {
            if (stockTopic.equals(record.topic()) && record.partition() == 0) {
                producer.sendMessage(orderTopic, 1, JsonUtil.convertObject2JsonString(order)); //create order successfully
                order.setOrderStatus("Tao don hang thanh con");         // do get product successfully
            } else if (stockTopic.equals(record.topic()) && record.partition() == 1) {
                order.setOrderStatus("Tao don hang that bai");          // do get product failed
            } else if (shippingTopic.equals(record.topic()) && record.partition() == 0) {
                order.setOrderStatus("Hang dang giao");                // do order've gone to finished
            } else if (shippingTopic.equals(record.topic()) && record.partition() == 1) {
                producer.sendMessage(orderTopic, 2, JsonUtil.convertObject2JsonString(order)); //create order failed
                order.setOrderStatus("Tao shipping that bai");        // create a shipment failed
            }
            return updateOrder(order);
        }
        return null;
    }
}
