package com.viettelimex.order_service.controller;

import com.viettelimex.common.api.AppResponseEntity;
import com.viettelimex.common.dto.Order;
import com.viettelimex.common.kafka.LogMarker;
import com.viettelimex.order_service.service.OrderService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {

    private static final Logger logger = LogManager.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @PostMapping("/do-order")
    public AppResponseEntity<?> doOrder(@RequestBody Order order) {
        logger.info(LogMarker.getMarker(), ": Hello world!");
        return AppResponseEntity.withSuccess("", "Ordering have already done!");
    }
}
