package com.viettelimex.shipping_service.repository;

import com.viettelimex.common.dto.Shipping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ShippingRepository extends JpaRepository<Shipping, String> {

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO shipping(shipping_id, shipping_at, shipping_code, shipping_name)" +
            " values (:shippingId, :shippingAt, :shippingCode, :shippingName)", nativeQuery = true)
    void createOne(@Param("shippingId") String shippingId,
                       @Param("shippingName") String shippingName,
                       @Param("shippingCode") String shippingCode,
                       @Param("shippingAt") String shippingAt);
}
