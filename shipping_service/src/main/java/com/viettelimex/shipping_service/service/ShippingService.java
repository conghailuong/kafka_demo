package com.viettelimex.shipping_service.service;


import com.viettelimex.common.dto.Shipping;

import java.util.List;

public interface ShippingService {

    Shipping createShipping(Shipping shipping);
}
