package com.viettelimex.shipping_service.service.serviceimpl;

import com.viettelimex.common.constant.DateTimeConstants;
import com.viettelimex.common.dto.Order;
import com.viettelimex.common.dto.Shipping;
import com.viettelimex.common.kafka.Producer;
import com.viettelimex.common.util.DateTimeUtils;
import com.viettelimex.common.util.JsonUtil;
import com.viettelimex.shipping_service.repository.ShippingRepository;
import com.viettelimex.shipping_service.service.ShippingService;
import com.viettelimex.shipping_service.service.base.BaseService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.stereotype.Service;

@Service
public class ShippingServiceImpl extends BaseService implements ShippingService {

    @Autowired
    private ShippingRepository shippingRepository;

    @Autowired
    private Producer<String> producer;

    @Override
    public Shipping createShipping(Shipping shipping) {
        return shippingRepository.save(shipping);
    }

    //    @KafkaListener(topics = {"create-order"})
    @KafkaListener(groupId = "viettlimex_2", containerFactory = "objectKafkaListenerContainerFactory",
            topicPartitions = {@TopicPartition(topic = "${erp.topic-name.order-service}", partitions = {"0"})})
    public void test(String value) {
        System.out.println("value: " + value);
    }

    /**
     * listen create success order event
     *
     * @param record
     * @return
     */
    //    @KafkaListener(topics = "create-order-success")
    @KafkaListener(topicPartitions = {@TopicPartition(topic = "${erp.topic-name.order-service}", partitions = {"1"})})
    private Shipping createShipping(ConsumerRecord<?, String> record) {
        Order order = JsonUtil.convertJson2Object(record.value(), Order.class);
        if (order != null) {
            try {
                shippingRepository.createOne("123",
                        "Van chuyen nhanh",
                        "KD904JFG",
                        DateTimeUtils.getCurrentDateString(DateTimeConstants.YYYY_MM_DD_HYPHEN));
                order.setShippingId("123");
                producer.sendMessage(shippingTopic, 0, JsonUtil.convertObject2JsonString(order)); // create a shipping successfully
            } catch (Exception e) {
                producer.sendMessage(shippingTopic, 1, JsonUtil.convertObject2JsonString(order)); // create a shipping failed
            }
        }
        return null;
    }

}
